# Server.py

import xbmc
import xbmcaddon
from xbmc import LOGNONE, LOGNOTICE
import math
import json
import sys
import socket
import select
import time
from thread import start_new_thread
sys.path.append('/storage/.kodi/addons/virtual.rpi-tools/lib')
import RPi.GPIO as GPIO

# User settings
__addon__ = xbmcaddon.Addon()
pwm_frequency = int(__addon__.getSetting("pwm_frequency"))

RED_LEDS = 17
GREEN_LEDS = 22
BLUE_LEDS = 24
DUTY_CYCLE_GRADE = float(float(20)/float(51))

HOST = '' 
SOCKET_LIST = []
RECV_BUFFER = 4096 
PORT = int(__addon__.getSetting("port"))

interrupt = 0

GPIO.setmode(GPIO.BCM)
GPIO.setup(RED_LEDS, GPIO.OUT)
GPIO.setup(GREEN_LEDS, GPIO.OUT)
GPIO.setup(BLUE_LEDS, GPIO.OUT)

pwm_red = GPIO.PWM(RED_LEDS, pwm_frequency)
pwm_green = GPIO.PWM(GREEN_LEDS, pwm_frequency)
pwm_blue = GPIO.PWM(BLUE_LEDS, pwm_frequency)

pwm_red.start(0)
pwm_green.start(0)
pwm_blue.start(0)

monitor = xbmc.Monitor()


def server():
    global interrupt
    interpolateRGB(0, 0, 0, 255, 0, 0, 100, 1)
    interpolateRGB(0, 0, 0, 0, 0, 255, 100, 1)
    interpolateRGB(0, 0, 0, 0, 255, 0, 100, 1)
    setRGB(0, 0, 0)

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((HOST, PORT))
    server_socket.listen(254)
 
    SOCKET_LIST.append(server_socket)

    print "[LEDStripeServer] Server started on port " + str(PORT)
    xbmc.log("[LEDStripeServer] Server started on port " + str(PORT), xbmc.LOGNOTICE)
 
    while not monitor.abortRequested():
        
        if monitor.waitForAbort(0.03):
            setRGB(0, 0, 0)
            break

        ready_to_read,ready_to_write,in_error = select.select(SOCKET_LIST,[],[],0)
      
        for sock in ready_to_read:
            if sock == server_socket: 
                sockfd, addr = server_socket.accept()
                SOCKET_LIST.append(sockfd)

                print "[LEDStripeServer] Client (%s, %s) connected" % addr
                xbmc.log("[LEDStripeServer] Client (%s, %s) connected" % addr, xbmc.LOGNOTICE)
            else:
                try:
                    data = sock.recv(RECV_BUFFER)
                    if data:
                        interrupt += 1
                        data = data.splitlines(True)[0]
                        print "[LEDStripeServer] Received value: " + data
                        try:
                            json_data = json.loads(data)
                            if 'light_effect' in json_data:
                                interrupt += 1
                                start_new_thread(parse_light_effect, (json_data['light_effect'], ))
                            else:
                                setRGB(json_data['setcolor']['r'], json_data['setcolor']['g'],
                                       json_data['setcolor']['b'])
                        except (ValueError, TypeError) as error:
                            print "[LEDStripeServer] JSON could not decoded" + error.message

                            if data[:3] == '999':
                                interrupt += 1
                                print("[LEDStripeServer] Receiving Skript:"+data)
                                start_new_thread(RGBscript, (data[3:],))    # data[3:]
                            else:
                                receiveRGB(server_socket, sock, data)
                        try:
                            if int(data) == -1:
                                interrupt += 1
                        except ValueError as e:
                            print "[LEDStripeServer] Data is not a valid Integer: " + e.message
                    else:    
                        if sock in SOCKET_LIST:
                            SOCKET_LIST.remove(sock)
                except socket.error:
                    print server_socket, sock, "[LEDStripeServer] Client (%s, %s) is offline\n" % addr
                    xbmc.log("[LEDStripeServer] Client (%s, %s) is offline\n" % addr, xbmc.LOGNOTICE)
                    continue

    server_socket.close()


def receiveRGB (server_socket, sock, message):
    print message
    if len(message) == 10:
        r = int(message[:3])
        g = int(message[3:6])
        b = int(message[6:])
    setRGB(r, g, b)


def setRGB(r, g, b):
    if r < 0:
        r = 0
    if g < 0:
        g = 0
    if b < 0:
        b = 0
    if r > 255:
        r = 255
    if g > 255:
        g = 255
    if b > 255:
        b = 255

    print "red: " + str(r), "green: " + str(g), "blue: " + str(b)

    pwm_red.ChangeDutyCycle(DUTY_CYCLE_GRADE * float(r))
    pwm_green.ChangeDutyCycle(DUTY_CYCLE_GRADE * float(g))
    pwm_blue.ChangeDutyCycle(DUTY_CYCLE_GRADE * float(b))


def interpolateRGB(r, g, b, r1, g1, b1, steps, time1):
    global interrupt
    thisThreadid = interrupt
    diff_r = float(r1 - r)
    diff_g = float(g1 - g)
    diff_b = float(b1 - b)
    diff_t = float(float(time1)/float(steps))
    trans_sum = float(interpolate_sum(steps))
    a_r = diff_r/trans_sum
    a_g = diff_g/trans_sum
    a_b = diff_b/trans_sum
    for x in xrange(0, steps):
        if interrupt != thisThreadid:
            break
        if diff_r > 0:
            r += a_r*x*x
        else:
            r += a_r*(steps-x)*(steps-x)
        
        if diff_g > 0:
            g += a_g*x*x
        else:
            g += a_g*(steps-x)*(steps-x)
        
        if diff_b > 0:
            b += a_b*x*x
        else:
            b += a_b*(steps-x)*(steps-x)
                
        setRGB(r, g, b)

        time.sleep(diff_t)


def interpolate_sum(steps):
    trans_sum = 0
    for i in range(0, steps):
        trans_sum += i*i
    return trans_sum


def linear_rgb_transition(r, g, b, r1, g1, b1, transition_time):
    global interrupt
    this_thread = interrupt
    time_per_step = 0.025
    steps = int(transition_time / time_per_step)
    a_r = float((r1 - r) / transition_time)
    a_g = float((g1 - g) / transition_time)
    a_b = float((b1 - b) / transition_time)
    n = 0
    h_r, h_g, h_b = 0, 0, 0
    c_r = a_r * time_per_step
    c_g = a_g * time_per_step
    c_b = a_b * time_per_step
    print "[LEDStripeServer] linear transition in " + str(transition_time) + " seconds with " + str(steps) + " steps."
    while n <= steps:
        if this_thread != interrupt:
            break
        time_before = time.time()
        r_neu = h_r + r
        g_neu = h_g + g
        b_neu = h_b + b
        setRGB(r_neu, g_neu, b_neu)
        n += 1
        h_r += c_r
        h_g += c_g
        h_b += c_b
        time_diff = time.time() - time_before
        print "[LEDStripeServer] time_diff: " + str(time_diff)
        time.sleep(time_per_step)


def exp_rgb_transition(r, g, b, r1, g1, b1, transition_time):
    global interrupt
    this_thread = interrupt
    time_per_step = 0.025
    diff_r = r1 - r
    diff_g = g1 - g
    diff_b = b1 - b
    if diff_r == 0:
        a_r = 0
    else:
        a_r = float(signum(diff_r) * (math.log(abs(diff_r)+1) / float(transition_time)))
    if diff_g == 0:
        a_g = 0
    else:
        a_g = float(signum(diff_g) * (math.log(abs(diff_g)+1) / float(transition_time)))
    if diff_b == 0:
        a_b = 0
    else:
        a_b = float(signum(diff_b) * (math.log(abs(diff_b)+1) / float(transition_time)))
    time_sum = 0.0
    while time_sum <= transition_time + time_per_step:  # comparison to equality doesn't work here, so we have to add
                                                        # one more step
        if this_thread != interrupt:
            break
        if diff_r >= 0:
            r_neu = math.exp(a_r * time_sum) + r - 1
        else:
            r_neu = math.exp(a_r * (time_sum - transition_time)) - 1 + r1
        if diff_g >= 0:
            g_neu = math.exp(a_g * time_sum) + g - 1
        else:
            g_neu = math.exp(a_g * (time_sum - transition_time)) - 1 + g1
        if diff_b >= 0:
            b_neu = math.exp(a_b * time_sum) + b - 1
        else:
            b_neu = math.exp(a_b * (time_sum - transition_time)) - 1 + b1
        setRGB(r_neu, g_neu, b_neu)
        time_sum += time_per_step
        time.sleep(time_per_step)


def RGBscript (command):
    print"[LEDStripeServer] New Thread started"
    pointer = 0
    global interrupt
    thisThreadid = interrupt
    while interrupt == thisThreadid:
        print "[LEDStripeServer] still here"
        if command[pointer] == '0':
            pointer+=1
            print "[LEDStripeServer] setFunktion"
            setRGB(int(command[pointer:3+pointer]), int(command[pointer+3:6+pointer]), int(command[6+pointer:9+pointer]))
            pointer+=9
        elif command[pointer] == '1':
            pointer+=1
            print "[LEDStripeServer] waitFunktion"
            print float(command[pointer:4+pointer])+float(command[pointer+4])*0.1+float(command[pointer+5])*0.01+float(command[pointer+6])*0.001+float(command[pointer+7])*0.0001 #Format 1234.5678
            time.sleep(float(command[pointer:4+pointer])+float(command[pointer+4])*0.1+float(command[pointer+5])*0.01+float(command[pointer+6])*0.001+float(command[pointer+7])*0.0001) 
            pointer+=8
        elif command[pointer] == '2':
            pointer+=1
            print "[LEDStripeServer] interpolateFunktion"
            print command[pointer:3+pointer],command[pointer+3:6+pointer],command[pointer+6:9+pointer],command[pointer+9:12+pointer],command[pointer+12:15+pointer],command[pointer+15:18+pointer], command[pointer+18:pointer+22], command[pointer+22:pointer+24]
            interpolateRGB(int(command[pointer:3+pointer]),int(command[pointer+3:6+pointer]),int(command[pointer+6:9+pointer]),int(command[pointer+9:12+pointer]),int(command[pointer+12:15+pointer]),int(command[pointer+15:18+pointer]), int(command[pointer+18:pointer+22]), int(command[pointer+22:pointer+24]))
            pointer+=24
        elif command[pointer] == 'E':
            pointer = 0
        else:
            print("[LEDStripeServer] ."+command[pointer]+".")
            print len(command)
            print("Didntd\n")
            interrupt = 1
    print"[LEDStripeServer] Stopped an old script-Thread"


def parse_light_effect(json_data):
    print "[LEDStripeServer] New Thread started"
    global interrupt
    this_threadid = interrupt
    while interrupt == this_threadid:
        for elem in json_data:
            str_elem = str(elem)
            print "[LEDStripeServer]Current JSON elem: " + str_elem
            if 'transition' in elem:
                trans_elem = elem['transition']
                start_color = trans_elem['startcolor']
                end_color = trans_elem['endcolor']
                print "[LEDStripeServer] Light Effect: Begin color transition:"
                print "\tStart Color: " + json.dumps(start_color)
                print "\tEnd Color: " + json.dumps(end_color)
                print "\tTime: " + json.dumps(trans_elem['time'])
                interpolateRGB(start_color['r'], start_color['g'], start_color['b'], end_color['r'], end_color['g'],
                               end_color['b'], int(trans_elem['time']/0.025), trans_elem['time'])
            elif 'wait' in elem:
                print "[LEDStripeServer] Light Effect: wait for " + json.dumps(elem['wait']) + " Seconds"
                wait_elem = elem['wait']
                time.sleep(wait_elem['time'])
            elif 'setcolor' in elem:
                print "[LEDStripeServer] Light Effect: changing color to " + json.dumps(elem)
                set_elem = elem['setcolor']
                setRGB(set_elem['r'], set_elem['g'], set_elem['b'])
            elif 'linear_transition' in elem:
                print "[LEDStripeServer] Light Effect: Starting linear transition"
                linear_elem = elem['linear_transition']
                start_color = linear_elem['startcolor']
                end_color = linear_elem['endcolor']
                linear_rgb_transition(start_color['r'], start_color['g'], start_color['b'], end_color['r'],
                                      end_color['g'], end_color['b'], linear_elem['time'])
            elif 'exp_transition' in elem:
                print "[LEDStripeServer] Light Effect: Starting exponential transition"
                exp_elem = elem['exp_transition']
                start_color = exp_elem['startcolor']
                end_color = exp_elem['endcolor']
                exp_rgb_transition(start_color['r'], start_color['g'], start_color['b'], end_color['r'], end_color['g'],
                                   end_color['b'], exp_elem['time'])


def signum(x):
    if x < 0:
        return -1.0
    return 1.0

 
if __name__ == "__main__":

    sys.exit(server())
