# Service Addon for LibreElec
## General
This is a service addon for **LibreElec** which wraps the  
**Server.py** file from the **LEDStripeServer** repository  
into a **Kodi Addon**.

### Important to know ###
To get this working you definitely need a **LED controller** which is connected to the Raspberry Pi's GPIO pins. 
Sadly no one offers a complete setup for sale but there is a awesome [guide](http://dordnung.de/raspberrypi-ledstrip/) 
from David Ordnung where he explains in detail how to build a LED controller on a breadboard.